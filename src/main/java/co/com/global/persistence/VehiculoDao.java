package co.com.global.persistence;

import java.sql.SQLException;

import org.springframework.stereotype.Component;

import co.com.global.persistence.entities.Vehiculo;

@Component
public interface VehiculoDao {
	public void save(Vehiculo vehiculo) throws SQLException;
}
