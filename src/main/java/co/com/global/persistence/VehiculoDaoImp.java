package co.com.global.persistence;

import java.sql.SQLException;

import javax.persistence.EntityManagerFactory;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import co.com.global.persistence.entities.Vehiculo;
@Component
public class VehiculoDaoImp implements VehiculoDao {
	
	@Autowired
	private EntityManagerFactory entityManagerFactory;
	
	@Override
	public void save(Vehiculo vehiculo) throws SQLException {
			Session session = entityManagerFactory.unwrap(SessionFactory.class).openSession();
			Transaction t = session.beginTransaction();
			session.persist(vehiculo);
			t.commit();
	}

}
