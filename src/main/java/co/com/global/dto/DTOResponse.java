package co.com.global.dto;

public class DTOResponse {

    private String descripcionError;
    private Boolean operacionExitosa;
    
    public DTOResponse() {
    	operacionExitosa = true;
    }

    public String getDescripcionError() {
        return descripcionError;
    }

    public void setDescripcionError(String descripcionError) {
        this.descripcionError = descripcionError;
    }

	public Boolean getOperacionExitosa() {
		return operacionExitosa;
	}

	public void setOperacionExitosa(Boolean operacionExitosa) {
		this.operacionExitosa = operacionExitosa;
	}



	public void procesoFallo() {
		this.operacionExitosa=false;
	}
}
