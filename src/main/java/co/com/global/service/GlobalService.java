package co.com.global.service;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.LinkedHashMap;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import co.com.global.dto.DTOResponse;
import co.com.global.persistence.VehiculoDao;
import co.com.global.persistence.entities.Vehiculo;

@Component
public class GlobalService {
	@Autowired
	VehiculoDao vehiculoDao;
	
	@Value( "${data.url}" )
	private String url;
	
	@Transactional
	public DTOResponse inicioProceso() {
		DTOResponse dtoResponse= new DTOResponse();
		try {
			List<Vehiculo> listaVehiculos=obtenerData();
			for(Vehiculo vehiculo: listaVehiculos) {
				vehiculoDao.save(vehiculo);
			}
			dtoResponse.setDescripcionError("Operacion exitosa");
			dtoResponse.setOperacionExitosa(true);
			return dtoResponse;
		}catch (Exception e) {
			dtoResponse.setDescripcionError("Error guardando la informacion: "+e.getMessage());
			dtoResponse.setOperacionExitosa(false);
			return dtoResponse;
		}
	}
	
	private List<Vehiculo> obtenerData() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {
	    RestTemplate restTemplate = new RestTemplate();
		ObjectMapper mapper = new ObjectMapper();
		URI uri = new URI(url);
		ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);
		String json =result.getBody();
		List<Vehiculo> listaVehiculos=mapper.readValue(json, new TypeReference<List<Vehiculo>>(){});
		return listaVehiculos;
	} 
	
}
