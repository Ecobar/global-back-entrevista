package co.com.global.controller;

import co.com.global.dto.DTOResponse;
import co.com.global.service.GlobalService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/global/proceso")
public class GlobalController {

	@Autowired
	GlobalService globalService;
	
    @GetMapping(path = "/inicio")
    public DTOResponse inicioProceso() {
        return globalService.inicioProceso();
    }
}
